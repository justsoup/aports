# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=mkinitfs
pkgver=3.11.0_rc3
# shellcheck disable=SC2034 # used for git versions, keep around for next time
_ver=${pkgver%_git*}
pkgrel=0
pkgdesc="Tool to generate initramfs images for Alpine"
url="https://gitlab.alpinelinux.org/alpine/mkinitfs"
arch="all"
license="GPL-2.0-only"
makedepends_host="busybox kmod-dev util-linux-dev cryptsetup-dev linux-headers"
makedepends="$makedepends_host"
checkdepends="kyua"
depends="
	apk-tools>=2.9.1
	busybox-binsh
	busybox>=1.28.2-r1
	kmod
	lddtree>=1.25
	mdev-conf
	"
subpackages="$pkgname-doc"
install="$pkgname.pre-upgrade $pkgname.post-install $pkgname.post-upgrade"
triggers="$pkgname.trigger=/lib/modules/*:/usr/lib/modules/*"
source="https://gitlab.alpinelinux.org/alpine/mkinitfs/-/archive/$pkgver/mkinitfs-$pkgver.tar.gz
	"

provides="initramfs-generator"
provider_priority=900 # highest

build() {
	make VERSION=$pkgver-r$pkgrel
}

check() {
	make check
}

package() {
	make install DESTDIR="$pkgdir"
}

sha512sums="
7015c87cfb7ae60f660d02774ab5f2be0704a564d58b893baa94dea801f654234024fe00dc466161b1b6e44fb9bd46f71bfdb38ad85592f73bd44b0377ebfe82  mkinitfs-3.11.0_rc3.tar.gz
"
